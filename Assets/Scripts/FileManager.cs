﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.MLAgents;
using UnityEditor;
using UnityEngine.Networking;
using Unity.Barracuda;
using System;
using System.Text.RegularExpressions;
using System.Linq;

public class FileManager : MonoBehaviour
{
    string path;
    public RawImage image;

    //para PC

    public void OpenExplorerPC()
    {
        path = EditorUtility.OpenFilePanel("Overwrite with png", "", "png,jpg");
        GetImage();
    }

    public void GetImage()
    {
        if (path != null)
        {
            UpdateImage();
        }
    }
    void UpdateImage()
    {
        WWW www = new WWW("file:///" + path);
        image.texture = www.texture;
    }

    //Para Android
    /*
	public void OpenExplorer(int maxSize)
	{
		NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
		{
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				// Create Texture from selected image
				Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					return;
				}

				// Assign texture to a temporary quad and destroy it after 5 seconds
				image.texture = texture;
			}
		}, "Select a PNG image", "image/png");

		Debug.Log("Permission result: " + permission);
	}
	*/
}
