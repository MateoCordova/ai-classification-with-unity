Original GIT: https://github.com/Syn-McJ/TFClassify-Unity-Barracuda

Train your own model: https://www.tensorflow.org/tutorials/images/classification?hl=es-419

Barracuda documentation:https://docs.unity3d.com/Packages/com.unity.barracuda@0.4/manual/index.html

Convertion tools: https://docs.microsoft.com/en-us/windows/ai/windows-ml/convert-model-winmltools

File exporer GIT: https://github.com/yasirkula/UnityNativeGallery
